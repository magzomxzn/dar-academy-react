import { Product } from './types';

export const data: Product[] = [
    {
        title: 'Makita / Дрель-шуруповерт аккумуляторная DF333DWYE4, 30Нм, 10.8В, 2х1.5Ач, з/у, кейс + набор B-28905',
        article: '8329681',
        description:
            'Дрель-шуруповерт аккумуляторная DF333DWYE4 имеет компактные размеры, малую массу и предназначена для сверления отверстий и установки крепежа.',
        image: 'https://images.wbstatic.net/big/new/8320000/8329681-1.jpg',
        price: 10000,
    },
    {
        title: 'Reebok / Кроссовки LIQUIFECT 180 3.0 CLGRY1/CBLACK/FUTTEA',
        article: '30030477',
        description:
            'Пробеги на километр больше. Эти кроссовки Reebok с полиуретановой вставкой на пятке делают каждый шаг мягким.',
        image: 'https://images.wbstatic.net/big/new/30030000/30030477-1.jpg',
        price: 5000,
    },
    {
        title: 'Philips / Автоматическая кофемашина LatteGo EP4343/EP4346',
        article: '14793410',
        description:
            'С полностью авотматической эспрессо-кофемашиной готовьте любимый кофе - эспрессо, кофе, капучино и латте маккиато - одним нажатием кнопки. ',
        image: 'https://images.wbstatic.net/big/new/14790000/14793410-1.jpg',
        price: 20000,
    },
    {
        title: 'Xiaomi / Смартфон Mi 11 Lite 8/128Gb',
        article: '27552442',
        description:
            'Невесомая конструкция| Элегантный и гладкий. Датчик отпечатков пальцев интегрирован в тонкую кнопку разблокировки, что обеспечивает более плавную разблокировку.',
        image: 'https://images.wbstatic.net/big/new/50420000/50423971-1.avif',
        price: 30000,
    },
    {
        title: 'Samsung / Телевизор Samsung UE50AU9010UXRU/50"/4K UHD/Smart TV/Wi-Fi/Bluetooth',
        article: '32175778',
        description:
            'Процессор UHD 4K - впечатляющее качество изображения благодаря обновленному процессору, который управляет цветопередачей, оптимизирует уровень контрастности и обеспечивает поддержку HDR.',
        image: 'https://images.wbstatic.net/big/new/32170000/32175778-1.jpg',
        price: 30000,
    },
];
