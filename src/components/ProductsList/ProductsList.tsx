import React from 'react';
import { Product } from '../../types';
import ProductItem from '../ProductItem/ProductItem';

type Props = {
    products: Product[];
};

const ProductsList: React.FC<Props> = ({ products }) => {
    const likeHandler = (item: Product) => {
        console.log(`Liked product with article ${item.article}`);
    };
    return (
        <div className="product-list">
            {products.map((product) => (
                <ProductItem
                    product={product}
                    key={product.article}
                    onLikeHandler={likeHandler}
                />
            ))}
        </div>
    );
};

export default ProductsList;
