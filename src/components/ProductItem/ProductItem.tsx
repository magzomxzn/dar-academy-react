import React from 'react';
import { Product } from '../../types';

type Props = {
    product: Product;
    onLikeHandler: (item: Product) => void;
};

const ProductItem: React.FC<Props> = ({ product, onLikeHandler }) => {
    return (
        <div className="product-item">
            <h3>{product.title}</h3>
            <small>{product.article}</small>
            <img src={product.image} style={{ width: '300px' }} />
            <p>{product.description}</p>
            <p>{product.price}</p>
            <button onClick={() => onLikeHandler(product)}>Like!</button>
        </div>
    );
};

export default ProductItem;
