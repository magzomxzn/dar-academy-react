import React, { useState } from 'react';
import ProductsList from '../../components/ProductsList/ProductsList';
import { data } from '../../mock';
import { Product } from '../../types';

const ProductsPage: React.FC = () => {
    const [products, setProduct] = useState<Product[]>(data);
    return (
        <div className="Products">
            <ProductsList products={products} />
        </div>
    );
};

export default ProductsPage;
